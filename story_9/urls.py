from django.contrib import admin
from django.urls import path
# views
from story_9.views import login, sign_up, logout

urlpatterns = [
    path('login/', login, name='login'),
    path('sign-up/', sign_up, name='sign-up'),
    path('logout/', logout, name='logout')
]