from django.shortcuts import render, redirect
from django.contrib import auth, messages
from django.http import HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponseForbidden

# Models
from django.contrib.auth.models import User

# Pages
def index(request):
  if request.user.is_authenticated:
    return render(request, 'story_9/index.html')
  return redirect('login-page')

def login_page(request):
  return render(request, 'story_9/login-page.html')

def sign_up_page(request):
  return render(request, 'story_9/sign-up-page.html')

# Endpoints
def login(request):
  if request.method == 'POST':
    try:
      username = request.POST['username']
      password = request.POST['password']
    except KeyError:
      return HttpResponseBadRequest()

    user = auth.authenticate(request=request, username=username, password=password)
    if user is not None:
      auth.login(request, user)
      return redirect('home')
    else:
      messages.error(request, 'Invalid credentials')
      return redirect('login-page')
  return HttpResponseNotAllowed(permitted_methods=['POST'])

def sign_up(request):
  if request.method == 'POST':
    try:
      username = request.POST['username']
      password = request.POST['password']
      email = request.POST['email']
    except KeyError:
      return HttpResponseBadRequest()

    try:
      username_used = User.objects.get(username=username)
    except User.DoesNotExist:
      username_used = None

    try:
      email_used = User.objects.get(email=email)
    except User.DoesNotExist:
      email_used = None

    if username_used or email_used:
      if username_used:
        messages.error(request, 'Username already exists')
      if email_used:
        messages.error(request, 'Email already exists')
      return redirect('sign-up-page')

    User.objects.create_user(username, email=email, password=password)
    return redirect('login-page')

  return HttpResponseNotAllowed(permitted_methods=['POST'])

def logout(request):
  auth.logout(request)
  return redirect('login-page')