window.addEventListener('load', () => {
  const form = document.querySelector('form')
  form.addEventListener('submit', (e) => {
    document.querySelectorAll('input').forEach(element => {
      if (!element.value) {
        e.preventDefault()
        element.classList.add('invalid')
        const small = document.createElement('small')
        small.className = 'error'
        small.innerHTML = `${element.name} is required`
        element.parentNode.insertBefore(small, element.nextSibling)
      }
    });
  })
})