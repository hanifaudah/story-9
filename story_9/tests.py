from django.test import TestCase, Client, RequestFactory
from django.contrib.auth import authenticate, login, logout, get_user
from django.contrib.messages import get_messages
from django.http import HttpRequest
from django.contrib.sessions.backends.db import SessionStore
from importlib import import_module
from django.contrib.messages.storage.fallback import FallbackStorage
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware

# Views
from story_9.views import index, login_page, login as user_login , sign_up_page, sign_up as user_sign_up, logout as user_logout

# Models
from django.contrib.auth.models import User

# Create your tests here.
class TestURLS(TestCase):
  def setUp(self):
    User.objects.create_user('username', email='email@email.com', password='password')

  def test_index_url_response_if_user_is_not_logged_in(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 302)

  def test_index_url_response_if_user_is_logged_in(self):
    self.client.login(username='username', password='password')
    response = self.client.get('')
    self.assertEquals(response.status_code, 200)

  def test_login_url_exists(self):
    response = Client().get('/login/')
    self.assertEquals(response.status_code, 200)

  def test_login_endpoint_exists(self):
    response = Client().get('/endpoint/login/')
    self.assertEquals(response.status_code, 405)

  def test_sign_up_url_exists(self):
    response = Client().get('/sign-up/')
    self.assertEquals(response.status_code, 200)

  def test_sign_up_endpoint_exists(self):
    response = Client().get('/endpoint/sign-up/')
    self.assertEquals(response.status_code, 405)

  def test_index_url_template(self):
    response = Client().get('')
    self.assertTemplateUsed('story_9/index.html')

  def test_login_url_template(self):
    response = Client().get('/login/')
    self.assertTemplateUsed(response, 'story_9/login-page.html')

  def test_sign_up_url_template(self):
    response = Client().get('/sign-up/')
    self.assertTemplateUsed(response, 'story_9/sign-up-page.html')

  def test_logout_url_exists(self):
    response = Client().get('/endpoint/logout/')
    self.assertEquals(response.status_code, 302)

class TestViews(TestCase):
  def setUp(self):
    User.objects.create_user('username', email='email@email.com', password='password')

  def test_index_view_response_if_user_is_not_logged_in(self):
    request = HttpRequest()
    request.user = User.objects.get(username='username')
    request.session = SessionStore(session_key=None)
    logout(request)
    response = index(request)
    self.assertEquals(response.status_code, 302)

  def test_index_view_response_if_user_is_logged_in(self):
    Client().login(username='username', password='password')
    request = HttpRequest()
    request.user = User.objects.get(username='username')
    response = index(request)
    self.assertEquals(response.status_code, 200)

  def test_login_endpoint_response_for_incomplete_credentials(self):
    request = HttpRequest()
    request.method = 'POST'
    response = user_login(request)
    self.assertEquals(response.status_code, 400)

  def test_login_endpoint_view_response_for_invalid_credentials(self):
    request = HttpRequest()
    request.method = 'POST'
    request.POST['username'] = 'username'
    request.POST['password'] = 'password_salah'

    # Middleware handling
    # Source: https://stackoverflow.com/questions/55843504/django-messages-middleware-issue-while-testing-post-request
    """Annotate a request object with a session"""
    middleware = SessionMiddleware()
    middleware.process_request(request)
    request.session.save()

    """Annotate a request object with a messages"""
    middleware = MessageMiddleware()
    middleware.process_request(request)
    request.session.save()

    response = user_login(request)
    self.assertEquals(response.status_code, 302)

  def test_login_endpoint_view_response_for_valid_credentials(self):
    request = HttpRequest()
    request.session = SessionStore(session_key=None)
    request.method = 'POST'
    request.POST['username'] = 'username'
    request.POST['password'] = 'password'
    response = user_login(request)
    self.assertEquals(response.status_code, 302)

  def test_login_endpoint_view_invalid_request_method(self):
    response = user_login(HttpRequest())
    self.assertEquals(response.status_code, 405)

  def test_login_page_view(self):
    response = login_page(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_sign_up_endpoint_view_response_for_invalid_data(self):
    request = HttpRequest()
    request.method = 'POST'
    response = user_sign_up(request)
    self.assertEquals(response.status_code, 400)

  def test_sign_up_endpoint_view_response_for_valid_data(self):
    request = HttpRequest()
    request.method = 'POST'
    request.POST['username'] = 'username1'
    request.POST['password'] = 'password'
    request.POST['email'] = 'emailUnique@email.com'

    # Middleware handling
    # Source: https://stackoverflow.com/questions/55843504/django-messages-middleware-issue-while-testing-post-request
    """Annotate a request object with a session"""
    middleware = SessionMiddleware()
    middleware.process_request(request)
    request.session.save()

    """Annotate a request object with a messages"""
    middleware = MessageMiddleware()
    middleware.process_request(request)
    request.session.save()

    response = user_sign_up(request)
    self.assertEquals(response.status_code, 302)

  def test_sign_up_endpoint_view_invalid_request_method(self):
    response = user_sign_up(HttpRequest())
    self.assertEquals(response.status_code, 405)

  def test_sign_up_endpoint_for_non_unique_username(self):
    User.objects.create_user('username1')
    request = HttpRequest()
    request.method = 'POST'
    request.POST['username'] = 'username1'
    request.POST['password'] = 'password'
    request.POST['email'] = 'email@email.com'

    # Middleware handling
    # Source: https://stackoverflow.com/questions/55843504/django-messages-middleware-issue-while-testing-post-request
    """Annotate a request object with a session"""
    middleware = SessionMiddleware()
    middleware.process_request(request)
    request.session.save()

    """Annotate a request object with a messages"""
    middleware = MessageMiddleware()
    middleware.process_request(request)
    request.session.save()

    response = user_sign_up(request)
    self.assertEquals(response.status_code, 302)

  def test_sign_up_endpoint_for_non_unique_email(self):
    User.objects.create_user('username2', email='email1@email.com')
    request = HttpRequest()
    request.method = 'POST'
    request.POST['username'] = 'username1'
    request.POST['password'] = 'password'
    request.POST['email'] = 'email1@email.com'

    # Middleware handling
    # Source: https://stackoverflow.com/questions/55843504/django-messages-middleware-issue-while-testing-post-request
    """Annotate a request object with a session"""
    middleware = SessionMiddleware()
    middleware.process_request(request)
    request.session.save()

    """Annotate a request object with a messages"""
    middleware = MessageMiddleware()
    middleware.process_request(request)
    request.session.save()

    response = user_sign_up(request)
    self.assertEquals(response.status_code, 302)


  def test_sign_up_page_view(self):
    response = sign_up_page(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_logout_endpoint_view_response(self):
    request = HttpRequest()
    request.session = SessionStore(session_key=None)
    response = user_logout(request)
    self.assertAlmostEquals(response.status_code, 302)
